import sklearn
import numpy as np
import pandas as pd
import tensorflow as tf

import nltk


import pickle
import os
from utilities import hobby_preprocess
from utilities import bert_preprocess
from flask import Flask, request, jsonify


from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from skmultilearn.problem_transform import BinaryRelevance, ClassifierChain, LabelPowerset
from skmultilearn.adapt import MLkNN
from sklearn.feature_extraction.text import CountVectorizer



with open("hobbies_classifier.obj", 'rb') as hf:
    hobbies_classifier = pickle.load(hf)

with open('kmeans_clusterer.obj', 'rb') as f:
    kmeans_clusterer = pickle.load(f)
 
with open('count_vectorizer.obj', 'rb') as cv_open:
    vectorizer = pickle.load(cv_open)

with open("PersonalityModel.obj", 'rb') as po:
    personality_classifier = pickle.load(po)





app = Flask(__name__)

@app.route("/")
def home():
    return "Welcome to our Tinder clone!"
    
@app.route("/predict_hobbies")
def predict_hobbies():

    text = request.args.get('text')    
    text = hobby_preprocess(text)

    doc_term_matrix = vectorizer.transform([text])
    
    results = hobbies_classifier.predict_proba(doc_term_matrix)
    print("RESULTS: ", results)
    
    cluster = kmeans_clusterer.predict(results)
    
    return jsonify(results=str(results[0]), cluster=int(cluster))

@app.route("/predict_personalities")
def predict_personalities():

    text = request.args.get('text')
    text = bert_preprocess(text)

    results = personality_classifier.predict([text])

    return jsonify(results=str(results[0]))
if __name__ == "__main__":
    app.run(debug=True)
   


